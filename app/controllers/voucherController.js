// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo models
const voucherModel = require("../model/voucherModel");

// create new voucher
const createVoucher = (req, res) => {
    //B1: Thu thập dữ liệu
    let bodyVoucher = req.body;
    console.log(bodyVoucher);
    // Kiểm tra maVoucher
    if (!bodyVoucher.maVoucher) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "maVoucher is not valid!"
        });
    }
    // Kiểm tra phanTramGiamGia
    if (!bodyVoucher.phanTramGiamGia || isNaN(bodyVoucher.phanTramGiamGia) || bodyVoucher.phanTramGiamGia < 0 || bodyVoucher.phanTramGiamGia > 100) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "phanTramGiamGia is not valid!"
        })
    }
    //B3: thực hiện tạo mới drink
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: bodyVoucher.maVoucher,
        phanTramGiamGia: bodyVoucher.phanTramGiamGia,
        ghiChu: bodyVoucher.ghiChu
    }
    voucherModel.create(newVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new voucher successfully!",
                "data": data
            })
        }
    })
};
// get all voucher
const getAllVoucher = (req, res) => {
    //B1: Thu thập dữ liệu(không cần)
    //B2: Kiểm tra dữ liệu(không cần)
    //B3: Thực hiện load all voucher
    voucherModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get all voucher successfully!",
                "data": data
            })
        }
    })
}

// get a voucher by id
const getVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    console.log(voucherId);
    let bodyVoucher = req.body;
    console.log(bodyVoucher);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Drink Id is not valid!"
        })
    }
    //B3: Load dữ liệu voucher theo id
    voucherModel.findById(voucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get a voucher by id successfully!",
                "data": data
            })
        }
    })
};
// update a voucher by Id
const updateVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    console.log(voucherId);
    let bodyVoucher = req.body;
    console.log(bodyVoucher);
    //B2: Kiểm tra dữ liệu
    // KIểm tra voucher Id
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Voucher Id is not valid!"
        })
    }
    // Kiểm tra maVoucher
    if (!bodyVoucher.maVoucher) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "maVoucher is not valid!"
        });
    }
    // Kiểm tra phanTramGiamGia
    if (!bodyVoucher.phanTramGiamGia || isNaN(bodyVoucher.phanTramGiamGia) || bodyVoucher.phanTramGiamGia < 0 || bodyVoucher.phanTramGiamGia > 100) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "phanTramGiamGia is not valid!"
        })
    }
    //B3: thực hiện tạo mới drink theo id
    let newVoucher = {
        maVoucher: bodyVoucher.maVoucher,
        phanTramGiamGia: bodyVoucher.phanTramGiamGia,
        ghiChu: bodyVoucher.ghiChu
    }
    voucherModel.findByIdAndUpdate(voucherId, newVoucher, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update a voucher by id successfully!",
                "data": data
            })
        }
    })
};
// delete voucher by id
const deleteVoucherById = (req, res) => {
    //B1: Thu thập dữ liệu
    const voucherId = req.params.voucherId;
    //B2: Kiểm tra dữ liệu
    // KIểm tra voucher Id
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Voucher Id is not valid!"
        })
    }
    //B3: Thực hiện xóa voucher theo id
    voucherModel.findByIdAndDelete(voucherId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete a voucher by id successfully!",
                "data": data
            })
        }
    })
};
module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}