// khai báo thư viện mongoose
const mongoose = require("mongoose");

// Khai báo models
const orderModel = require("../model/orderModel");
const userModel = require('../model/userModel');
const { updateUserById } = require('./userController');

// Khai báo thư viện random
var randomToken = require('random-token');

// create new order
const createOrder = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);

    const voucherId = req.body.voucherId;
    console.log(voucherId);

    const drinkId = req.body.drinkId;
    console.log(drinkId);

    const orderCode = randomToken(8); // Thực hiện random Ordercode
    console.log(orderCode); // vdq5qas7
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Drink Id is not valid!"
        });
    }
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Voucher Id is not valid!"
        });
    }
    if (!body.pizzaSize) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "pizzaSize is not valid!"
        });
    }
    if (!body.pizzaType) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "pizzaType is not valid!"
        });
    }
    if (!body.status) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "status is not valid!"
        });
    }
    //B3: Thực hiện cơ sở dữ liệu
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: voucherId,
        drink: drinkId,
        status: body.status
    }
    orderModel.create(newOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Create new order successfully",
                data: data
            })
        }
    })
};

// Get all order
const getAllOrder = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện load all order
    orderModel
        .find()
        .sort({ pizzaType: "asc" })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Load all order successfully!",
                    "data": data
                })
            }
        })
};

// Get order by id
const getOrderById = (req, res) => {
    //B1: Thu thập dữ liệu
    const orderId = req.params.orderId;
    console.log(orderId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Order Id is not valid!"
        })
    }
    // B3: Thực hiện load user theo id
    orderModel
        .findById(orderId)
        .populate("voucher")
        .populate("drink")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    status: "Error 500: internal server error",
                    message: err.message
                })
            }
            else {
                return res.status(201).json({
                    status: "Get order by Id successfully!",
                    "data": data
                })
            }
        })
};

// Update order by id
const updateOrder = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);

    const orderCode = randomToken(8);
    console.log(orderCode);

    const voucherId = req.body.voucherId;
    console.log(voucherId);

    const drinkId = req.body.drinkId;
    console.log(drinkId);

    const orderId = req.body.orderId;
    console.log(orderId);

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Order Id is not valid!"
        });
    }
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Drink Id is not valid!"
        });
    }
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Voucher Id is not valid!"
        });
    }
    if (!body.orderCode) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "orderCode is not valid!"
        });
    }
    if (!body.pizzaSize) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "pizzaSize is not valid!"
        });
    }
    if (!body.pizzaType) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "pizzaType is not valid!"
        });
    }
    if (!body.status) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "status is not valid!"
        });
    }
    //B3: Thực hiện cơ sở dữ liệu
    const updateOrder = {
        orderCode: orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: voucherId,
        drink: drinkId,
        status: body.status
    }
    orderModel.findByIdAndUpdate(updateOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update order by id successfully",
                data: data
            })
        }
    })
};

// Delete a order by id
const deleteOrder = (req, res) => {
    //B1: Thu thập dữ liệu
    const orderId = req.params.orderId;
    console.log(orderId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Order Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    orderModel.findByIdAndDelete(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Delete order by Id successfully!",
                data: data
            })
        }
    })
};

// create new order
const createOrderHandle = (req, res) => {
    //B1: Thu thập dữ liệu
    const body = req.body;
    console.log(body);

    const email = req.body.email;

    const orderCode = randomToken(8); // Thực hiện random Ordercode
    console.log(orderCode); // vdq5qas7
    //B2: Kiểm tra dữ liệu

    //B3: Thực hiện cơ sở dữ liệu
    userModel.findOne({ email: email }, (err, emailExists) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            if (emailExists) { // email đã tồn tại trong hệ thống, thực hiện lấy id user, tạo new order và update dữ liệu vào user
                
                const userId = emailExists._id; // lấy id user từ response trả về
                console.log(userId);

                const newOrder = {
                    _id: mongoose.Types.ObjectId(),
                    orderCode: orderCode,
                    pizzaSize: "M",
                    pizzaType: "Hải Sản",
                    voucher: mongoose.Types.ObjectId(),
                    drink: mongoose.Types.ObjectId(),
                    status: "Open"
                }
                orderModel.create(newOrder, (err, orderCreated) => {
                    if (err) {
                        return res.status(500).json({
                            status: "Error 500: internal server error",
                            message: err.message
                        })
                    }
                    userModel.findByIdAndUpdate(userId, { $push: { orders: orderCreated._id } }, (err, updateUserById) => {
                        if (err) {
                            return res.status(500).json({
                                status: "Error 500: internal server error",
                                message: err.message
                            })
                        }
                        else {
                            return res.status(201).json({
                                status: "Create new order successfully!",
                                data: orderCreated
                            })
                        }
                    })
                })
            }
            else { // Nếu email chưa tồn tại, thực hiện tạo user mới

                const newUser = {
                    _id: mongoose.Types.ObjectId(),
                    fullName: body.fullName,
                    email: body.email,
                    address: body.address,
                    phone: body.phone
                }
                // Kiểm tra dữ liệu
                // Kiểm tra fullName
                if (!body.fullName) {
                    return res.status(400).json({
                        status: "Error 400: bad request",
                        message: "fullName is not valid!"
                    });
                }
                // Kiểm tra email
                if (!body.email || !validateEmail(body.email)) {
                    return res.status(400).json({
                        status: "Error 400: bad request",
                        message: "Email is not valid!"
                    });
                }
                // Kiểm tra address
                if (!body.address) {
                    return res.status(400).json({
                        status: "Error 400: bad request",
                        message: "Address is not valid!"
                    });
                }
                // Kiểm tra số điện thoại
                if (!body.phone || !validatePhone(body.phone)) {
                    return res.status(400).json({
                        status: "Error 400: bad request",
                        message: "Phone is not valid!"
                    });
                }
                // Thực hiện cơ sở dữ liệu
                userModel.create(newUser, (err, dataUser) => {
                    if (err) {
                        return res.status(500).json({
                            status: "Error 500: internal server error",
                            message: err.message
                        })
                    }
                    else { 
                        // Lấy id của user vừa được tạo
                        const newUserId = dataUser._id;
                        console.log(newUserId);

                        // Tạo new order thêm vào mảng orders của user
                        const newOrder = {
                            _id: mongoose.Types.ObjectId(),
                            orderCode: orderCode,
                            pizzaSize: "M",
                            pizzaType: "Hải Sản",
                            voucher: mongoose.Types.ObjectId(),
                            drink: mongoose.Types.ObjectId(),
                            status: "Open"
                        }
                        orderModel.create(newUserId, (err, orderCreated) => {
                            if (err) {
                                return res.status(500).json({
                                    status: "Error 500: internal server error",
                                    message: err.message
                                })
                            }
                            // Thêm Id của order vừa tạo ra vào mảng orders của user
                            userModel.findByIdAndUpdate(newUserId, {$push: {orders: orderCreated._id}}, (err, updateUserById) => {
                                if (err) {
                                    return res.status(500).json({
                                        status: "Error 500: internal server error",
                                        message: err.message
                                    })
                                }
                                else {
                                    return res.status(201).json({
                                        status: "Create new order successfully!",
                                        data: orderCreated
                                    })
                                }
                            })
                        })
                    }
                })
            }
        }
    })
}
// hàm kiểm tra định dạng email
const validateEmail = (paramEmail) => {
    "use strict";
    var atposition = paramEmail.indexOf("@");
    var dotposition = paramEmail.lastIndexOf(".");
    if (atposition < 1 || dotposition < (atposition + 2) || (dotposition + 2) >= paramEmail.length) {
        return false;
    }
    return true;
}
// hàm kiểm tra định dạng số điện thoại
const validatePhone = (number) => {
    "use strict";
    var vform = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
    if (vform.test(number)) {
        return true;
    }
    else {
        return false;
    }
};

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrder,
    deleteOrder,
    createOrderHandle
}