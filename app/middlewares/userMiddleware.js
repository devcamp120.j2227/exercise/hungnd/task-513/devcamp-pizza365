const getAllUserMiddleware = (req, res, next) => {
    console.log("Get All User!");
    next();
};

const getUserMiddleware = (req, res, next) => {
    console.log("Get a User!");
    next();
};

const postUserMiddleware = (req, res, next) => {
    console.log("Create a User!");
    next();
};

const putUserMiddleware = (req, res, next) => {
    console.log("Update a User!");
    next();
};

const deleteUserMiddleware = (req, res, next) => {
    console.log("Delete a User!");
    next();
};

// Get all limit user
const getAllLimitUser = (req, res, next) => {
    console.log("Get all limit user");
    next();
}
module.exports = {
    getAllUserMiddleware,
    getUserMiddleware,
    postUserMiddleware,
    putUserMiddleware,
    deleteUserMiddleware,
    getAllLimitUser
}