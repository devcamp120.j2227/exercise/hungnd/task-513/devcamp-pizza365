// get All Order
const getAllOrderMiddleware = (req, res, next) => {
    console.log("Get All Order!");
    next();
};
// Get a order
const getOrderMiddleware = (req, res, next) => {
    console.log("Get a Order!");
    next();
};
// create new order
const createOrderMiddleware = (req, res, next) => {
    console.log("Create a Order!");
    next();
};
// update order
const updateOrderMiddleware = (req, res, next) => {
    console.log("Update a Order!");
    next();
};
// delete order
const deleteOrderMiddleware = (req, res, next) => {
    console.log("Delete a Order!");
    next();
};

module.exports = {
    getAllOrderMiddleware,
    getOrderMiddleware,
    createOrderMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}