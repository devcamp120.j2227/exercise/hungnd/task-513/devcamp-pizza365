// khai báo thư viện express
const express = require('express');

// Import middleware
const {
    getAllDrinksMiddleware,
    getDrinksMiddleware,
    postDrinksMiddleware,
    putDrinksMiddleware,
    deleteDrinksMiddleware
} = require('../middlewares/drinkMiddleware');
// tạo ra Router
const drinkRouter = express.Router();

//Import drink controller
const drinkController = require('../controllers/drinkController');

// Create a drink
drinkRouter.post("/drinks", postDrinksMiddleware, drinkController.createDrink);
// get all drink
drinkRouter.get('/drinks', getAllDrinksMiddleware, drinkController.getAllDrink);
// get a drink
drinkRouter.get('/drinks/:drinkId', getDrinksMiddleware, drinkController.getDrinkById);
// update drink
drinkRouter.put('/drinks/:drinkId', putDrinksMiddleware, drinkController.updateDrinkById);
// Delete a drink
drinkRouter.delete('/drinks/:drinkId', deleteDrinksMiddleware, drinkController.deleteDrinkById);
module.exports = {
    drinkRouter
}
