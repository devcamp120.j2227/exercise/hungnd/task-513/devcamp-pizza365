// Khai báo thư viện express
const express = require('express');

// Import orderMiddleware
const orderMiddleware = require('../middlewares/orderMiddleware');

// Tạo ra router
const orderRouter = express.Router();

// Import orderController
const orderController = require('../controllers/orderController');

// Create order
orderRouter.post("/orders", orderMiddleware.createOrderMiddleware, orderController.createOrder);

// get all order
orderRouter.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

// get order by id
orderRouter.get("/orders/:orderId", orderMiddleware.getOrderMiddleware, orderController.getOrderById);

// update order by id
orderRouter.put("/orders/:orderId", orderMiddleware.createOrderMiddleware, orderController.updateOrder);

// delete order by id
orderRouter.delete("/orders/:orderId", orderMiddleware.deleteOrderMiddleware, orderController.deleteOrder);

// thực hiện tạo order bằng cách tìm kiếm email của user
orderRouter.post("/devcamp-pizza365/orders", orderController.createOrderHandle);

module.exports = { orderRouter };
