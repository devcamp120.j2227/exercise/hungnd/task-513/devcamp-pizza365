// khai báo thư viện express
const express = require('express');

// Import userMiddleware
const userMiddleware = require('../middlewares/userMiddleware');

// tạo ra userRouter
const userRouter = express.Router();

// Import userController
const userController = require('../controllers/userController');

// Create new user
userRouter.post('/users', userMiddleware.postUserMiddleware, userController.createUser);

// Get all user
userRouter.get('/users', userMiddleware.getAllUserMiddleware, userController.getAllUser);

// Get user by id
userRouter.get('/users/:userId', userMiddleware.getUserMiddleware, userController.getUserById);

// Update user by id
userRouter.put('/users/:userId', userMiddleware.putUserMiddleware, userController.updateUserById);

// Delete user by id
userRouter.delete('/users/:userId', userMiddleware.deleteUserMiddleware, userController.deleteUserById);

// get all user limit
userRouter.get('/limit-users', userMiddleware.getAllLimitUser, userController.getAllLimitUser);

// get all user skip
userRouter.get('/skip-users', userMiddleware.getAllUserMiddleware, userController.getAllSkipUser);

// get all user skip - limit
userRouter.get('/skip-limit-users', userMiddleware.getAllUserMiddleware, userController.getAllLimitSkipUser);

// get all user sort - skip - limit
userRouter.get('/sort-skip-limit-users', userMiddleware.getAllUserMiddleware, userController.getAllSortLimitSkipUser);

module.exports = {
    userRouter
}