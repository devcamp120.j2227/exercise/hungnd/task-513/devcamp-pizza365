// Import thư viện expressjs tương đương import express from "express";
const express = require("express");

// Import thư viện path
const path = require("path");

// Khởi tạo 1 app express
const app = express();

// khai báo thư viện mongoose
const mongoose = require("mongoose");

// khai báo cổng chạy project
const port = 8000;

// Khai báo để sử dụng bodyJson
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Thư viện random
var randomToken = require('random-token');

// Khai báo API dạng /
app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/Pizza 365 v1.9 Menu.html"));
});

//Import drinkRouter
const { drinkRouter } = require('./app/routes/drinkRouter');
app.use("/", drinkRouter);

//Import voucher Router
const { voucherRouter } = require('./app/routes/voucherRouter');
app.use('/', voucherRouter);

//Import userRouter và sử dụng
const {userRouter} = require('./app/routes/userRouter');
app.use("/", userRouter);

// Import orderRouter và sử dụng
const { orderRouter } = require('./app/routes/orderRouter');
app.use("/", orderRouter);

// Kết nối với mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});

app.use(express.static(__dirname + "/views"));

app.listen(port, () => {
    console.log("App listening on port: ", port);
});